Look at the main.dart
is very simple to understand.

Steps:
1. Import image_picker plugin
2. Call ImagePicker classes
3. Enjoy



Implement image picker in flutter using library with example

Image Picker flutter library
A Flutter Plugin library for both iOS and Android that is been used to pick an images from mobile phone gallery or even you can take a new photo with the camera.

let’s straight away start implement the flutter image picker in our flutter app.


1. find the latest version of Image picker from official site. (https://pub.dev/packages/image_picker)

Then Following Steps:- 


2. Step 1: Adding image picker dependency flutter in to our flutter project
Once you have created a new Flutter project or open your existing flutter project to implement image picker flutter library.

Then you need to add image picker dependencies,

Open pubspec.yaml  then add the below lines

"" dependencies:
  image_picker: ^0.6.3+4 //add this line in pubspec.yaml file  ""

Step 2: Import image_picker.dart file in main.dart
Now, as you have added the required library in your project now you need to import the picker library wherever required.

import 'package:image_picker/image_picker.dart';

Step 3: Configuration
1. ANDROID IMAGE PICKER CONFIGURATION

When it comes to android their is no configuration required – the plugin should work out of the box.

Just we need to set request Legacy External Storage to true Android > app > src > main >AndroidManifest.xml

By adding the above request Legacy External Strorage the app will auto ask user to grant permission to make use of storage.

2. IOS IMAGE PICKER CONFIGURATION

Add the following keys to your Info.plist file, located in

Project > ios > Runner > Info.plist

Copy paste the below permission in info.plist

<key>NSPhotoLibraryUsageDescription</key>
<string>This app requires read and write permission Photo Library</string>
used to ask uses to give permission for accessing photo library. This is Called Privacy – Photo Library Usage Description.

<key>NSCameraUsageDescription</key>
<string>This app requires read and write permission from the user.</string>
Used to take photo from device camera itself.

<key>NSMicrophoneUsageDescription</key>
<string>This app requires get access to micro phone</string>
if you intend to record an video by using camera, video offcourse need audio to be recorded, this permission is used to record audio from microphone.

Now in code 

import 'package:image_picker/image_picker.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  File _image;

  Future getCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _image = image;
    });
  }

  Widget bottomsheet() {
    return Container(
      height: 100,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Column(
        children: <Widget>[
          Text(
            "Choose pic",
            style: TextStyle(fontSize: 20.0),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            children: <Widget>[
              FlatButton.icon(
                icon: Icon(Icons.camera),
                onPressed: getCamera,
                label: Text('Camera'),
              ),
              FlatButton.icon(
                icon: Icon(Icons.folder),
                onPressed: getGallerypic,
                label: Text('Gallery'),
              )
            ],
          )
        ],
      ),
    );
  }

  Future getGallery() async {
    // var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      showModalBottomSheet(
        context: context,
        builder: ((builder) => bottomsheet()),
      );
      // _image = image;
    });
  }

  Future getGallerypic() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Image Picker Example'),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.landscape,
            ),
            onPressed: getGallery,
          ),
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () {
              setState(() {
                _image = null;
              });
            },
          )
        ],
      ),
      body: Center(
        child: _image == null ? Text('Nothing to show.') : Image.file(_image),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: getCamera,
        child: Icon(Icons.camera_alt),
      ),
    );
  }
}


